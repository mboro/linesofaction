package boardElements;

import java.awt.Color;
import java.util.ArrayList;

public class Pawn {
	
		private Position position;  
		private Player owner;
		private Board board;
		
		public static final int WIDTH = 40;
		
		
		public Pawn(Position pos, Player own, Board brd){
			owner = own;
			position = pos;
			board = brd;
			
			owner.pawns.add(this);
		}
		public boolean moveTo(Position pos){
			position = pos;
			return true;
			
		}
		
		public void getPossibleMoves(){
			
		}
		
		public ArrayList<Pawn> getNeighbors(){
			
			ArrayList<Pawn> neighbors = new ArrayList<Pawn>();
			Pawn demPawn;
			
			if(position.getX() > 0){ // na lewo
				demPawn = board.getField(new Position(position.getX() - 1, position.getY()));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getX() < 7){ //na prawo
				demPawn = board.getField(new Position(position.getX() + 1, position.getY()));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getY() > 0){ //na gorze
				demPawn = board.getField(new Position(position.getX(), position.getY() - 1));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getY() < 7){ //na dole
				demPawn = board.getField(new Position(position.getX(), position.getY() + 1));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getY() < 7 && position.getX() < 7){ //dol, prawo
				demPawn = board.getField(new Position(position.getX() + 1, position.getY() + 1));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getY() > 0 && position.getX() > 0){ //gora, lewo
				demPawn = board.getField(new Position(position.getX() - 1, position.getY() - 1));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getY() < 7 && position.getX() > 0){ //dol, lewo
				demPawn = board.getField(new Position(position.getX() - 1, position.getY() + 1));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			if(position.getY() > 0 && position.getX() < 7){ //gora, prawo
				demPawn = board.getField(new Position(position.getX() + 1, position.getY() - 1));
				if(demPawn != null && demPawn.getOwner() == owner)neighbors.add(demPawn);
			}
			
			return neighbors;
		}
		
		public Color getColor(){
			return owner.getPawnColor();
		}
		
		public Player getOwner(){return owner;}
		public Position getPosition(){
			return position;
		}
}


