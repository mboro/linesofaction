package boardElements;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;

import main.LOAManager;

public class Board {

	private Pawn board[][];
	private Player black;
	private Player white;
	private LOAManager loamanager;
	
	private Player activePlayer;
	
	private Player winner;
	private Position winningMoveEnd;
	private Position winningMoveBeginning; //in fact same as selectedField
	
	public Position selectedField;
	private ArrayList<Position> availableMoves = new ArrayList<Position>();
	
	private int clickState;
	private static final int PAWN_NOT_SELECTED = 0;
	private static final int PAWN_SELECTED = 1;
	
	public static final int FIELD_WIDTH = 50;
	
	public Board(LOAManager loam){ //huehuehue
		
		loamanager = loam;
		
		board = new Pawn[8][8];
		black = new Player(Color.BLACK, "Black");
		white = new Player(Color.WHITE, "White");
		
	}
	
	public void initializeBoard(){
		
		winner = null;
		winningMoveEnd = null;
		winningMoveBeginning = null;
		
		activePlayer = black;
		
		for(int i=1; i<7; i++)board[i][0] = new Pawn(new Position(i,0),white, this);
		for(int i=1; i<7; i++)board[i][7] = new Pawn(new Position(i,7),white, this);
		
		for(int i=1; i<7; i++)board[0][i] = new Pawn(new Position(0,i),black, this);
		for(int i=1; i<7; i++)board[7][i] = new Pawn(new Position(7,i),black, this);
		
		//board[0][0] = new Pawn(new Position(0,0), null, this);
		
	}
	
	public Pawn getField(Position pos){
		return board[pos.getX()][pos.getY()];
	}
	
	public void boardClicked(Position posPixelFromBoardLTCorner){
		
		if(winner != null){
			
			loamanager.endGame(winner.getName());
			resetBoard();
			
		}
		
		Position pos = new Position(posPixelFromBoardLTCorner);
		
		//calculate the field
		int fieldX = pos.getX() / FIELD_WIDTH;
		int fieldY = pos.getY() / FIELD_WIDTH;
		
//		System.out.println(fieldX + "  " + fieldY);
		
		if(clickState == PAWN_NOT_SELECTED && board[fieldX][fieldY] != null && board[fieldX][fieldY].getOwner() == activePlayer){
			
			selectedField = new Position(fieldX, fieldY);
			clickState = PAWN_SELECTED;
			generateAvailableMoves(new Position(fieldX, fieldY), board[fieldX][fieldY].getOwner());
			
			
		} else if(clickState == PAWN_SELECTED && selectedField.getX() == fieldX && selectedField.getY() == fieldY){
			
			clickState = PAWN_NOT_SELECTED;
			selectedField = null;
			
		} else if(clickState == PAWN_SELECTED && board[fieldX][fieldY] != null 
				&& board[fieldX][fieldY].getOwner() == board[selectedField.getX()][selectedField.getY()].getOwner()) {
			
			System.out.println("Invalid move!");
			
		}
		else if(clickState == PAWN_SELECTED){
			
			if(board[fieldX][fieldY] != null){
				
				System.out.println("Destroying ennemy pawn!");
				board[fieldX][fieldY].getOwner().pawns.remove(board[fieldX][fieldY]);
				
			}
			
			boolean validMove = false;
			for(Position avPos : availableMoves){
				
				if(avPos.getX() == fieldX && avPos.getY() == fieldY){
					
					validMove = true;	
					
				}
			}
			
			if(validMove == true){
				
				board[fieldX][fieldY] = board[selectedField.getX()][selectedField.getY()];
				board[selectedField.getX()][selectedField.getY()] = null;
				
				board[fieldX][fieldY].moveTo(new Position(fieldX, fieldY));
				
				clickState = PAWN_NOT_SELECTED;
				if(checkForVictory(black)){
					
					System.out.println(black.getName() + " won!");
					winner = black;
					
					winningMoveBeginning = new Position(selectedField);
					winningMoveEnd = new Position(fieldX, fieldY);
					
					//resetBoard();
					//loamanager.endGame(black.getName());
					
				} else if(checkForVictory(white)){
					
					System.out.println(white.getName() + " won!");
					winner = white;
					
					winningMoveBeginning = new Position(selectedField);
					winningMoveEnd = new Position(fieldX, fieldY);
					
					//resetBoard();
					//loamanager.endGame(white.getName());
					
				} else {
					nextTurn();
				}
			}
		}
	}
	
	private void resetBoard(){
		
		for(int i = 0; i < 8; i++)
			for(int j = 0; j < 8; j++){
				board[i][j] = null;
			}
		
		white.clearPawns();
		black.clearPawns();
		
		initializeBoard();
	}
	
	private int traverseBlob(Pawn p, ArrayList<Pawn> visited){
		
		int result = 0;
		
		ArrayList<Pawn> neighbors = p.getNeighbors();
		
		for(Pawn vi : visited){
			if(neighbors.contains(vi))neighbors.remove(vi);
		}
		
		if(neighbors.isEmpty())return 1;
		
		for(Pawn pz : neighbors){
				
			if(!visited.contains(pz)){
				//System.out.println("entering " + pz.getPosition().getX() + ":" + pz.getPosition().getY());
				visited.add(pz); 
				result += traverseBlob(pz, visited);
			}
		}
		
		return result + 1;
	}
	private boolean checkForVictory(Player player){
		
		int allPawns = player.pawns.size();
		int countedPawns = 0;
		
		for(Pawn p : player.pawns){
			
			ArrayList<Pawn> visited = new ArrayList<Pawn>();
			visited.add(p);
			countedPawns = traverseBlob(p, visited);
			
			break; //only one chain of detections
		}
		
		if(countedPawns == allPawns)return true;
		else return false;
		
	}
	
	private void nextTurn(){
		if(activePlayer == white)activePlayer = black;
		else activePlayer = white;
	}
	
	private void generateAvailableMoves(Position pawnPosition, Player pawnOwner){
		
		availableMoves.clear();
		
		int bottomLeftTopRight = 0;
		int bottomRightTopLeft = 0;
		int leftRight = 0;
		int bottomTop = 0;
		
		int topRightBoundary = Math.min(7 - pawnPosition.getX(), pawnPosition.getY());
		int topLeftBoundary = Math.min(pawnPosition.getX(), pawnPosition.getY());
		int bottomRightBoundary = Math.min(7 - pawnPosition.getX(), 7 - pawnPosition.getY());
		int bottomLeftBoundary = Math.min(pawnPosition.getX(), 7 - pawnPosition.getY());
		int leftBoundary = pawnPosition.getX();
		int rightBoundary = 7-pawnPosition.getX();
		int topBoundary = pawnPosition.getY();
		int bottomBoundary = 7-pawnPosition.getY();
		
		for(int i = 1; i < 8 - pawnPosition.getX(); i++){ //approaching to left side of the board
			
			if(board[pawnPosition.getX() + i][pawnPosition.getY()] != null){
				
				leftRight++;
				
				if(rightBoundary == 7 - pawnPosition.getX() 
						&& board[pawnPosition.getX() + i][pawnPosition.getY()].getOwner() != pawnOwner){
					
					rightBoundary = i;
					
				}
			}
						
			if(pawnPosition.getY() + i < 8 && board[pawnPosition.getX() + i][pawnPosition.getY() + i] != null){
				
						bottomRightTopLeft++;
					
						if(bottomRightBoundary == Math.min(7 - pawnPosition.getX(), 7 - pawnPosition.getY())
								&& board[pawnPosition.getX() + i][pawnPosition.getY() + i].getOwner() != pawnOwner){
							
							bottomRightBoundary = i;
							
						}
					}
			
			if(pawnPosition.getY()-i >= 0 && board[pawnPosition.getX() + i][pawnPosition.getY() - i] != null){
						
						bottomLeftTopRight++;
						
						if(topRightBoundary == Math.min(7 - pawnPosition.getX(), pawnPosition.getY())
								&& board[pawnPosition.getX() + i][pawnPosition.getY() - i].getOwner() != pawnOwner){
							
							topRightBoundary = i;
							
						}
			}
			
			
		}
		
		for(int i = 0; i <= pawnPosition.getX(); i++){
			
			if(board[pawnPosition.getX() - i][pawnPosition.getY()] != null){
				
				leftRight++;
				
				if(leftBoundary == pawnPosition.getX()
						&& board[pawnPosition.getX() - i][pawnPosition.getY()].getOwner() != pawnOwner){
					
					leftBoundary = i;
				}
			}
			
			if(pawnPosition.getY() - i >= 0 && board[pawnPosition.getX() - i][pawnPosition.getY() - i] != null){
				
					bottomRightTopLeft++;
					
					if(topLeftBoundary == Math.min(pawnPosition.getX(), pawnPosition.getY())
							&& board[pawnPosition.getX() - i][pawnPosition.getY() - i].getOwner() != pawnOwner){
						topLeftBoundary = i;
					}
				
				}
			
			
				if(pawnPosition.getY()+i < 8 && board[pawnPosition.getX() - i][pawnPosition.getY() + i] != null){
					
					bottomLeftTopRight++;
					
					if(bottomLeftBoundary == Math.min(pawnPosition.getX(), 7 - pawnPosition.getY())
							&& board[pawnPosition.getX() - i][pawnPosition.getY() + i].getOwner() != pawnOwner){
						
						bottomLeftBoundary = i;
						
					}
				}
			
		}
		
		for(int i = 1; i < 8-pawnPosition.getY(); i++){
			if(board[pawnPosition.getX()][pawnPosition.getY()+i] != null){
				bottomTop++;
				
				if(bottomBoundary == 7-pawnPosition.getY()
						&& board[pawnPosition.getX()][pawnPosition.getY()+i].getOwner() != pawnOwner){
					
					bottomBoundary = i;
				}
			}
		}
		
		for(int i = 0; i <= pawnPosition.getY(); i++){
			
			if(board[pawnPosition.getX()][pawnPosition.getY()-i] != null){
				bottomTop++;
				
				if(topBoundary == pawnPosition.getY()
						&& board[pawnPosition.getX()][pawnPosition.getY()-i].getOwner() !=pawnOwner){
					
					topBoundary = i;
				}
			}
		}
		
		if(bottomRightTopLeft != 0 && bottomRightTopLeft <= topLeftBoundary
				&& (board[pawnPosition.getX() - bottomRightTopLeft][pawnPosition.getY() - bottomRightTopLeft] == null
				||  board[pawnPosition.getX() - bottomRightTopLeft][pawnPosition.getY() - bottomRightTopLeft].getOwner() != pawnOwner)){
			
			availableMoves.add(new Position(pawnPosition.getX() - 
				bottomRightTopLeft, pawnPosition.getY() - bottomRightTopLeft));
			
		}
		
		if(bottomRightTopLeft != 0 && bottomRightTopLeft <= bottomRightBoundary
				&& (board[pawnPosition.getX() + bottomRightTopLeft][pawnPosition.getY() + bottomRightTopLeft] == null
				||  board[pawnPosition.getX() + bottomRightTopLeft][pawnPosition.getY() + bottomRightTopLeft].getOwner() != pawnOwner)){
			
			availableMoves.add(new Position(pawnPosition.getX() +
				bottomRightTopLeft, pawnPosition.getY() + bottomRightTopLeft));
			
		}
				
		if(bottomLeftTopRight != 0 && bottomLeftTopRight <= topRightBoundary
				&& (board[pawnPosition.getX() + bottomLeftTopRight][pawnPosition.getY() - bottomLeftTopRight] == null
				||  board[pawnPosition.getX() + bottomLeftTopRight][pawnPosition.getY() - bottomLeftTopRight].getOwner() != pawnOwner))
		{
			availableMoves.add(new Position(pawnPosition.getX() +
				bottomLeftTopRight, pawnPosition.getY() - bottomLeftTopRight));
		}
		
		if(bottomLeftTopRight != 0 && bottomLeftTopRight <= bottomLeftBoundary
				&& (board[pawnPosition.getX() - bottomLeftTopRight][pawnPosition.getY() + bottomLeftTopRight] == null
				||  board[pawnPosition.getX() - bottomLeftTopRight][pawnPosition.getY() + bottomLeftTopRight].getOwner() != pawnOwner))
		{
			availableMoves.add(new Position(pawnPosition.getX() -
				bottomLeftTopRight, pawnPosition.getY() + bottomLeftTopRight));
		}
		
		if(leftRight != 0 && leftRight <= rightBoundary 
				&& (board[pawnPosition.getX() + leftRight][pawnPosition.getY()] == null
					|| board[pawnPosition.getX() + leftRight][pawnPosition.getY()].getOwner() != pawnOwner))
		{
			availableMoves.add(new Position(pawnPosition.getX() +
				leftRight, pawnPosition.getY()));
		}
		
		if(leftRight != 0 && leftRight <= leftBoundary 
				&& (board[pawnPosition.getX() - leftRight][pawnPosition.getY()] == null
				    || board[pawnPosition.getX() - leftRight][pawnPosition.getY()].getOwner() != pawnOwner))
		{
			availableMoves.add(new Position(pawnPosition.getX() -
				leftRight, pawnPosition.getY()));
		}
		
		if(bottomTop != 0 && bottomTop <= bottomBoundary
				&& (board[pawnPosition.getX()][pawnPosition.getY() + bottomTop] == null
					|| board[pawnPosition.getX()][pawnPosition.getY() + bottomTop].getOwner() != pawnOwner))
		{
			availableMoves.add(new Position(pawnPosition.getX(),
				pawnPosition.getY() + bottomTop));
		}
		
		if(bottomTop != 0 && bottomTop <= topBoundary
				&& (board[pawnPosition.getX()][pawnPosition.getY() - bottomTop] == null
				 	|| board[pawnPosition.getX()][pawnPosition.getY() - bottomTop].getOwner() != pawnOwner))
		{
			availableMoves.add(new Position(pawnPosition.getX(),
				pawnPosition.getY() - bottomTop));
		}
		
	}
	
	public void drawBoard(Graphics2D g, int boardCornerX, int boardCornerY){
		g.fillRect(boardCornerX, boardCornerY, 400, 400);
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				
				if((j%2==0 && i%2==0) || (j%2==1 && i%2==1))g.setColor(new Color(85,105,164));
				else g.setColor(new Color(113,145,236));
				
				g.fillRect(boardCornerX + j*50, boardCornerY + i*50, 50, 50);

			}
		}
		
		if(clickState == Board.PAWN_SELECTED){
			
			g.setColor(Color.RED);
			g.fillRect(boardCornerX + selectedField.getX()*Board.FIELD_WIDTH, boardCornerY + selectedField.getY()*Board.FIELD_WIDTH, Board.FIELD_WIDTH, Board.FIELD_WIDTH);
			
			g.setColor(Color.GREEN);
			
			
			
			if(availableMoves != null)
			for(Position pos : availableMoves){
				g.fillRect(boardCornerX + pos.getX()*Board.FIELD_WIDTH, boardCornerY + pos.getY()*Board.FIELD_WIDTH, Board.FIELD_WIDTH, Board.FIELD_WIDTH);
			}
			
		}
		
		if(winner != null){
			g.setColor(Color.YELLOW);
			g.fillRect(boardCornerX + winningMoveBeginning.getX()*Board.FIELD_WIDTH, boardCornerY + winningMoveBeginning.getY()*Board.FIELD_WIDTH, Board.FIELD_WIDTH, Board.FIELD_WIDTH);
			g.fillRect(boardCornerX + winningMoveEnd.getX()*Board.FIELD_WIDTH, boardCornerY + winningMoveEnd.getY()*Board.FIELD_WIDTH, Board.FIELD_WIDTH, Board.FIELD_WIDTH);
		}
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){

				if(board[j][i] != null){
					g.setColor(board[j][i].getColor());
					g.fillOval(boardCornerX + j*50+5,  boardCornerY + i*50+5, 40, 40);
					
				}
			}
		}
	}
	
	public int getClickState(){return clickState;}
}
