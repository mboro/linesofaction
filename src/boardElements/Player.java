package boardElements;

import java.awt.Color;
import java.util.ArrayList;

public class Player {
	
	private final Color pawnColor;
	private String name;
	public ArrayList<Pawn> pawns = new ArrayList<Pawn>();
	
	public Player(){
		//int randomNum = ninimum + (int)(Math.random()*maximum);
		
		int r = 0 + (int)(Math.random() * 255);
		int g = 0 + (int)(Math.random() * 255);
		int b = 0 + (int)(Math.random() * 255);
		
		pawnColor = new Color(r,g,b);
	}
	public Player(Color c, String name){
		pawnColor = c;
		this.name = name;
	}
	
	public Color getPawnColor(){
		return pawnColor;
	}
	
	public String getName(){return name;}
	public void clearPawns(){
		pawns.clear();
	}
}
