package boardElements;

public class Position {
	
	private int posX, posY; //counted as fields on board (NOT pixels);
	
	public Position(){
		posX = 0;
		posY = 0;
	}
	
	public Position(Position p){
		posX = p.getX();
		posY = p.getY();
	}
	
	public Position(int x, int y){
		posX = x;
		posY = y;
	}
	
	public boolean setPosition(int x, int y){
		posX = x;
		posY = y;
		return true;
	}
	
	public boolean setPosition(Position p){
		return setPosition(p.getX(), p.getY());
	}
	
	public int getX(){return posX;}
	public int getY(){return posY;}
}
