package main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

import display.GameEnd;
import display.Menu;

import boardElements.*;

import engine.GameManager;
import engine.GraphicsEngine;
public class LOAManager implements GameManager{

		private int pawnX, pawnY;
		private int winHeight, winWidth;
		
		private GameEnd gameEnd;
		private Menu menu;
		private String winner;
		private JFrame okno;
		private GraphicsEngine graphicsEngine;
		
		private int displayState;
		private static final int DISPLAY_MENU = 0;
		private static final int DISPLAY_GAME = 1;
		private static final int DISPLAY_GAME_END = 2;
		
		private Board board;
		
		public LOAManager(){
			
			pawnX = pawnY = 10;
			board = new Board(this);
			board.initializeBoard();
			
			menu = new Menu(this);
			gameEnd = new GameEnd(this);
			
			displayState = DISPLAY_MENU;
		}
		
	public void closeWindow(){
		if(okno != null && graphicsEngine != null){
			
			graphicsEngine.shutDown();
			//okno.setVisible(false);
			//okno.dispose();
			
			
		}
	}
	public void setGameEngine(GraphicsEngine ge){
		
		graphicsEngine = ge;
		
	}
    @Override
	public void renderGame(Graphics2D g, JFrame window) throws IOException {
	
    	okno = window;
    	
    	if(displayState == DISPLAY_MENU){
    		//displayState = DISPLAY_GAME;
    		
    		menu.drawMenu(g, window);
    		
    	} else if(displayState == DISPLAY_GAME_END){

    		gameEnd.drawGameEnd(g, window, winner);
    		
    	} else if(displayState == DISPLAY_GAME){
    	
	    	int width = winWidth = window.getWidth();
	    	int height = winHeight = window.getHeight();
	    	
			g.setColor(Color.BLACK);
	    	g.fillRect(0, 0, width, height);
	    	
	    	int boardCornerX = (width-400)/2 - 10;
	    	int boardCornerY = (height-400)/2 - 15;
	    	
	    	board.drawBoard(g, boardCornerX, boardCornerY);
	    	
    	}
    	//BufferedImage pawnImg = ImageIO.read(new File("pawn.jpg"));
    	//g.drawImage(pawnImg, null, pawnX, pawnY); 
		
	}

	@Override
	public void updateGame() {

		//System.out.println("Updating game.");
		
	}
	
	
	public void startGame(){
		if(displayState == DISPLAY_MENU)displayState = DISPLAY_GAME;
	}
	
	public void endGame(String winner){
		this.winner = winner;
		if(displayState == DISPLAY_GAME)displayState = DISPLAY_GAME_END;
	}
	
	public void showMenu(){
		if(displayState == DISPLAY_GAME_END)displayState = DISPLAY_MENU;
	}
	@Override
	public void onClick(int x, int y) {
		
		if(displayState == DISPLAY_MENU){
			
			menu.onClick(x, y);
			
		} else if(displayState == DISPLAY_GAME_END){
			
			gameEnd.onClick(x, y);
			
		} else if(displayState == DISPLAY_GAME) {
			
			int boardCornerX = (winWidth-400)/2 - 10;
	    	int boardCornerY = (winHeight-400)/2 - 15;
	    	
	    	if(x >= boardCornerX && x <= boardCornerX + 400
	    			&& y >= boardCornerY && y <= boardCornerY + 400){
	    			
	    		board.boardClicked(new Position(x - boardCornerX, y - boardCornerY));
	    		
	    	}
	    	
		}
	}

}
