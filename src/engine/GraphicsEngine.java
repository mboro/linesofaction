package engine;

import java.io.IOException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;


	public class GraphicsEngine extends JPanel implements Runnable, MouseListener{
		
		private Thread t;
	    private boolean isRunning = true;
	    private GameManager gameManager;
	    private JFrame frame;
	    
	    private int width = 640;
	    private int height = 480;
	    private int scale = 1;

	    // Setup
	    public void shutDown(){
	    	isRunning = false;
	    }
	    public GraphicsEngine(GameManager gm) {
	    	
	    	//callback
	    	gameManager = gm;
	    	gm.setGameEngine(this);
	    	
	    	//listener
	    	addMouseListener(this);
	    	
	    	// JFrame
	    	frame = new JFrame("Lines Of Action");
	    	frame.addWindowListener(new FrameClose());
	    	frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	    	frame.setSize(width * scale, height * scale);
	    	frame.setVisible(true);
	    	frame.add(this);
	    	
	    	ImageIcon img = new ImageIcon("icon.png");
	    	frame.setIconImage(img.getImage());
	    	
	    	//Thread
	    	t = new Thread(this);
	    	t.start();
	    }

	    private class FrameClose extends WindowAdapter {
	    	@Override
	    	public void windowClosing(final WindowEvent e) {
	    		isRunning = false;
	    	}
	    }
  
	    public void paintComponent(Graphics g){
	    	
	    	Graphics2D g2 = (Graphics2D) g;
	    	
	    	try {
				gameManager.renderGame(g2, frame);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // this calls your draw method
	    }

	    public void run() {
	    	//backgroundGraphics = (Graphics2D) background.getGraphics();
	    	long fpsWait = (long) (1.0 / 30 * 1000);
	    	
	    	while (isRunning) {
	    		
	    		long renderStart = System.nanoTime();
	    		gameManager.updateGame();

	    		repaint();

	    		// Better do some FPS limiting here
	    		long renderTime = (System.nanoTime() - renderStart) / 1000000;
	    		
	    		try {
	    			
	    			Thread.sleep(Math.max(0, fpsWait - renderTime));
	    			
	    		} catch (InterruptedException e) {
	    			
	    			Thread.interrupted();
	    			break;
	    			
	    		}
	    		
	    		renderTime = (System.nanoTime() - renderStart) / 1000000;

	    	}
	    	frame.dispose();
	    }

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent me) {
			
			gameManager.onClick(me.getX(), me.getY());
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	}


	