package engine;

import java.awt.Graphics2D;
import java.io.IOException;

import javax.swing.JFrame;

public interface GameManager {
	
	public void renderGame(Graphics2D g, JFrame window) throws IOException;
	public void setGameEngine(GraphicsEngine ge);
	public void updateGame();
	public void onClick(int x, int y);
}
