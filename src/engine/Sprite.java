package engine;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

//not used

public class Sprite {
	
	private BufferedImage spriteImage;
	
	public Sprite(String filePath) throws IOException{
		
		spriteImage = ImageIO.read(new File(filePath));
		
	}
	
}
