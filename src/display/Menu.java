package display;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import main.LOAManager;

public class Menu {
	
		private LOAManager loamanager;
		//private int cornerX, cornerY;
		private int width, height;
		private int cornerX, cornerY;
		
		public Menu(LOAManager loam){
			loamanager = loam;
			width = 400;
			height = 450;
			cornerX = 0;
			cornerY = 0;
		}
	  	public void drawMenu(Graphics2D g, JFrame window) throws IOException{
	  		int winWidth = window.getWidth();
	    	int winHeight = window.getHeight();
			g.setColor(Color.BLACK);
			
	    	g.fillRect(0, 0, winWidth, winHeight);
	    	
	    	cornerX = (winWidth-width)/2 - 10;
	    	cornerY = (winHeight-height)/2 - 15;
	    	
	    	//g.setColor(Color.WHITE);
	    	//g.fillRect(cornerX, cornerY, width, height);
	    	
	    	BufferedImage menuImage = ImageIO.read(new File("menu.png"));
	    	g.drawImage(menuImage, cornerX, cornerY, null);
	    	
	    	g.setColor(Color.BLACK);
	    	
	    	//Font czcionka = new Font("Helvetica", Font.BOLD, 30);
	    	//g.setFont(czcionka);
	    	//g.drawString("Lines Of Action!", cornerX + 90, cornerY + 40);
	    }
	  	
	  	public void onClick(int x, int y){
	  		
	  		if(isInBox(x, y, 90, 170, 300, 200)){
	  		
	  			loamanager.startGame();
	  			
	  		} else if(isInBox(x, y, 90, 220, 300, 245)){
	  			
	  			System.out.println("<Unsupported> Human vs. Computer");
	  			
	  		} else if(isInBox(x, y, 80, 270, 300, 290)){
	  			
	  			System.out.println("<Unsupported> Computer vs. Computer");
	  			
	  		} else if(isInBox(x, y, 150, 315, 235, 335)){
	  			
	  			loamanager.closeWindow();
	  			
	  		}
	  		
	  	}
	    
	  	public boolean isInBox(int clickX, int clickY, int boxLeftTopX, int boxLeftTopY, int boxRightBottomX, int boxRightBottomY){
	  		
	  		if(clickX >= boxLeftTopX + cornerX && clickX <= boxRightBottomX + cornerX 
	  				&& clickY >= boxLeftTopY + cornerY && clickY <= boxRightBottomY + cornerY){
	  			
	  			return true;
	  			
	  		}
	  		
	  		return false;
	  		
	  	}
}
