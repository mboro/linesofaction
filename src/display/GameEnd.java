package display;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import javax.swing.JFrame;

import main.LOAManager;

import boardElements.Position;

public class GameEnd {
	
		private LOAManager loamanager;
		
		private int width, height;
		
		public GameEnd(LOAManager loam){
			loamanager = loam;
			width = 400;
			height = 400;
			
		}
	    
	    public void drawGameEnd(Graphics2D g, JFrame window, String winner){
	    	int winWidth = window.getWidth();
	    	int winHeight = window.getHeight();
			g.setColor(Color.BLACK);
			
	    	g.fillRect(0, 0, winWidth, winHeight);
	    	
	    	int cornerX = (winWidth-width)/2 - 10;
	    	int cornerY = (winHeight-height)/2 - 15;
	    	
	    	g.setColor(Color.WHITE);
	    	
	    	Font czcionka = new Font("Helvetica", Font.BOLD, 30);
	    	g.setFont(czcionka);
	    	
	    	g.drawString("The winner is... " + winner + "!", cornerX + 40, cornerY + 200);
	    }
	    
	    public void onClick(int x, int y){
	  		
	  		loamanager.showMenu();
	  		
	  	}
	    
	    
}
